﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankGeek.Model
{
    [Table("UserFibonacci")]
    public class UserFibonacci
    {
  
        [Required]
        [Key]
        public int Id { get; set; }
        [MaxLength(5)]
        public string UserName { get; set; }
        [MaxLength(5)]
        public int FirstNumber { get; set; }
        [MaxLength(5)]
        public int SecondNumber { get; set; }
        public bool? BooleanFibonacci { get; set; }
        public int ResultNumber { get; set; }

    }
}
