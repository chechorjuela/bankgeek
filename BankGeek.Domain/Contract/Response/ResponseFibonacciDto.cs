﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankGeek.Domain.Contract.Response
{
    public class ResponseFibonacciDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }
        public bool? BooleanFibonacci { get; set; }
        public int ResultNumber { get; set; }
    }
}
