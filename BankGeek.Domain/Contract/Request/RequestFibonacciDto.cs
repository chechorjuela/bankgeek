﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankGeek.Domain.Contract.Request
{
    public class RequestFibonacciDto
    {
        public string UserName { get; set; }
        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }
    }
}