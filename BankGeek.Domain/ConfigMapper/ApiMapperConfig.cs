﻿using AutoMapper;
using BankGeek.Domain.Contract.Request;
using BankGeek.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankGeek.Domain.ConfigMapper
{
    public class ApiMappingProfile : Profile
    {
        public ApiMappingProfile()
        {
            CreateMap<UserFibonacci, RequestFibonacciDto>().ReverseMap();
        }
    }
}
