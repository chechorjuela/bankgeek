﻿using AutoMapper;
using BankGeek.Domain.Contract.Request;
using BankGeek.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankGeek.Domain
{
    public class ConfigAutomapper
    {
        public MapperConfiguration CreateMaps()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => {
                cfg.CreateMap<UserFibonacci, RequestFibonacciDto>();
            });
            return config;
        }
    }
}
