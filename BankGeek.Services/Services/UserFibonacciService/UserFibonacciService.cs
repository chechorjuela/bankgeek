﻿using AutoMapper;
using BankGeek.Domain;
using BankGeek.Domain.Contract.Request;
using BankGeek.Domain.Contract.Response;
using BankGeek.Model;
using BankGeek.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BankGeek.Services.Services.UserFibonacciService
{
    public class UserFibonacciService : IUserFibonacciService
    {
        private readonly IUserFibonacciRepository _userFibonaRepository;
        private readonly MapperConfiguration configMapper;
        private readonly IMapper _mapper;
        public UserFibonacciService(IUserFibonacciRepository userFibonacciService, IMapper mapper)
        {
            _userFibonaRepository = userFibonacciService;
        
            _mapper = mapper;
        }
        public List<ResponseFibonacciDto> GetAll() => _mapper.Map<List<ResponseFibonacciDto>>(_userFibonaRepository.GetAll());
        
        public ResponseFibonacciDto CreateFibonacci(RequestFibonacciDto request)
        {
            UserFibonacci userF = _mapper.Map<UserFibonacci>(request);
            int resultSum = request.FirstNumber + request.SecondNumber;
            userF.BooleanFibonacci = isFibonacci(resultSum);
            userF.ResultNumber = resultSum;
            var response = _mapper.Map<ResponseFibonacciDto>(_userFibonaRepository.Create(userF));
            return response;
        }
        public bool isFibonacci(int n)
        {
            return isPerfectSquare(5 * n * n + 4) ||
                   isPerfectSquare(5 * n * n - 4);
        }
        public bool isPerfectSquare(int x)
        {
            int s = (int)Math.Sqrt(x);
            return (s * s == x);
        }
    }
}
