﻿using BankGeek.Domain.Contract.Request;
using BankGeek.Domain.Contract.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankGeek.Services.Services
{
    public interface IUserFibonacciService
    {
        ResponseFibonacciDto CreateFibonacci(RequestFibonacciDto request);
        List<ResponseFibonacciDto> GetAll();
        bool isFibonacci(int n);
        bool isPerfectSquare(int x);
    }
}
