﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankGeek.Repository.Entities.Base
{
    public interface IRepository<TModel>
    {
        TModel Get(int id);
        List<TModel> GetAll();
        TModel Add(TModel entity);
        TModel Remove(TModel entity);
        TModel Update(int id, TModel entity);
    }
}
