﻿using BankGeek.Repository.Core;
using BankGeek.Repository.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankGeek.Repository
{
    public class Repository<TModel> : IRepository<TModel> where TModel : class
    {
        protected readonly DataContext DataBaseContext;
        public Repository(DataContext context)
        {
            this.DataBaseContext = context;
        }
        public TModel Add(TModel entity)
        {
            DataBaseContext.Set<TModel>().Add(entity);
            DataBaseContext.SaveChanges();
            return entity;
        }

        public TModel Get(int id)
        {
            return DataBaseContext.Set<TModel>().Find(id);
        }

        public List<TModel> GetAll()
        {
            return DataBaseContext.Set<TModel>().ToList();
        }

        public TModel Remove(TModel entity)
        {
            var contextResponse = DataBaseContext.Set<TModel>().Remove(entity);
            DataBaseContext.SaveChanges();
            if (contextResponse != null)
            {
            }
            return entity;
        }

        public TModel Update(int id, TModel entity)
        {
            DataBaseContext.Set<TModel>().Update(entity);
            DataBaseContext.SaveChanges();
            return entity;
        }
    }
}
