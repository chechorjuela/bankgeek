﻿using BankGeek.Model;
using BankGeek.Repository.Entities.Base;


namespace BankGeek.Repository
{
    public interface IUserFibonacciRepository : IRepository<UserFibonacci>
    {
        UserFibonacci Create(UserFibonacci user);
    }
}
