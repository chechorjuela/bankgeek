﻿using BankGeek.Model;
using BankGeek.Repository.Core;

namespace BankGeek.Repository.Entities
{
    public class UserFibonacciRepository : Repository<UserFibonacci>, IUserFibonacciRepository
    {
        private DataContext _context;
        public UserFibonacciRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public UserFibonacci Create(UserFibonacci user)
        {
            _context.UserFibonacci.Add(user);
            _context.SaveChanges();
            return user;

        }
    }
}
