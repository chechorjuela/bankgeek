import { all, fork } from "redux-saga/effects";

import userSaga from "./UserRedux/sagas";

export function* rootSaga() {
  yield all([fork(userSaga)]);
}