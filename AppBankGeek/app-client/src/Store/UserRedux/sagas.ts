
import { all, call, put, takeLatest } from "redux-saga/effects";
import { fetchPostUserSuccess, fetchUserFailure, fetchUserSuccess } from "./actions";
import { FETCH_POST_USER_REQUEST, FETCH_USER_REQUEST, FETCH_USER_SUCCESS } from "./actionTypes";
import user from "../../Api/UserApi"
import { AxiosResponse } from "axios";


function* fetchUserSaga() {
    
  try {
    const users:AxiosResponse<any> = yield call(user.getUsers);
    yield put(
      fetchUserSuccess({
        users: users.data,
      })
    );
  } catch (e) {
    
  }
}
function* fetchPostUserSaga(userFibonacci:any) {
  try {
    const userPost:AxiosResponse<any> = yield call(user.postUser,{
        createUserViewModel: userFibonacci.user
    });
    yield put(
      fetchPostUserSuccess({
        user: userPost.data,
      })
    );
  } catch (e) {
    
  }
}
/*
  Starts worker saga on latest dispatched `FETCH_USER_REQUEST` action.
  Allows concurrent increments.
*/
function* userSaga() {
  yield all([takeLatest(FETCH_USER_REQUEST, fetchUserSaga),takeLatest(FETCH_POST_USER_REQUEST,fetchPostUserSaga)]);
}

export default userSaga;