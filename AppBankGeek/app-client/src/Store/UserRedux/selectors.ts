import { createSelector } from "reselect";
import { AppState } from "../rootReducer";

const getPending = (state: AppState) => state.user.pending;

const getUsers = (state: AppState) => state.user.users;

const getError = (state: AppState) => state.user.error;

const postUser = (state: AppState) => state.user.user;

export const getUsersSelector = createSelector(getUsers, (users: any) => users);

export const getPendingSelector = createSelector(
  getPending,
  (pending) => pending
);

export const getErrorSelector = createSelector(getError, (error: any) => error);

export const getUserSelector = createSelector(postUser, (user: any) => user);