import {
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    FETCH_USER_FAILURE,
    FETCH_POST_USER_REQUEST,
    FETCH_POST_USER_SUCCESS,
  } from "./actionTypes";
  
  import { UserActions, UserState } from "./types";
  
  const initialState: UserState = {
    pending: false,
    users: [],
    user:{},
    error: null,
    loading: false,
  };
  
export function userReducer(
    state = initialState,
    action: UserActions,
): UserState {
    switch (action.type) {
        case FETCH_USER_REQUEST:
            return { ...state, loading: true, pending:true }
        case FETCH_USER_SUCCESS:
            return { ...state, loading: false, users: action.payload.users, pending: false }
        case FETCH_USER_FAILURE:
            return { ...state, loading: false, pending: false, error: "Error Server" }
        case FETCH_POST_USER_REQUEST:
            return { ...state, pending: true}
        case FETCH_POST_USER_SUCCESS:
            return { ...state, pending: false, user:action.payload.user}
        default:
            return state
    }
}