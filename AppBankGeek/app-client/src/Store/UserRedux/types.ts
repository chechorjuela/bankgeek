import {
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    FETCH_USER_FAILURE,
    FETCH_POST_USER_REQUEST,
    FETCH_POST_USER_SUCCESS,
  } from "./actionTypes";
  
  export interface IUser {
    data?: any
  }
  
  export interface UserState {
    pending: boolean;
    users: any[];
    user: any;
    error: string | null;
    loading: Boolean;
  }
  
  export interface FetchUserSuccessPayload {
    users: IUser[];
  }
  export interface FetchPostUserSuccessPayload {
    user: IUser;
  }
  
  export interface FetchUserFailurePayload {
    error: string;
  }
  export interface FetchPostUserRequest {
    type: typeof FETCH_POST_USER_REQUEST;
    user : any;
  }
  export interface FetchUserRequest {
    type: typeof FETCH_USER_REQUEST;
  }
  
  export type FetchUserSuccess = {
    type: typeof FETCH_USER_SUCCESS;
    payload: FetchUserSuccessPayload;
  };
  export type FetchPostUserSuccess = {
    type: typeof FETCH_POST_USER_SUCCESS;
    payload: FetchPostUserSuccessPayload;
  };
  export type FetchUserFailure = {
    type: typeof FETCH_USER_FAILURE;
    payload: FetchUserFailurePayload;
  };
  export interface AddProductAction {
    type: typeof FETCH_POST_USER_REQUEST
    user: any
  }
  export type UserActions =
    | FetchPostUserRequest
    | FetchUserRequest
    | FetchUserSuccess
    | FetchPostUserSuccess
    | FetchUserFailure;