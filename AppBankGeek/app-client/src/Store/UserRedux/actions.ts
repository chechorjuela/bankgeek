import {
    FETCH_USER_REQUEST,
    FETCH_USER_FAILURE,
    FETCH_USER_SUCCESS,
    FETCH_POST_USER_REQUEST,
    FETCH_POST_USER_SUCCESS,
  } from "./actionTypes";

  import {
    FetchUserRequest,
    FetchUserSuccess,
    FetchUserSuccessPayload,
    FetchUserFailure,
    FetchUserFailurePayload,
    FetchPostUserRequest,
    FetchPostUserSuccess,
    FetchPostUserSuccessPayload,
  } from "./types";
  
  export const fetchUserRequest = (): FetchUserRequest => ({
    type: FETCH_USER_REQUEST,
  });
  export function addUser(user: any): FetchPostUserRequest {
    return {
      type: FETCH_POST_USER_REQUEST,
      user,
    }
  }
  export const fetchUserSuccess = (
    payload: FetchUserSuccessPayload
  ): FetchUserSuccess => ({
    type: FETCH_USER_SUCCESS,
    payload,
  });
  export const fetchPostUserSuccess = (
    payload: FetchPostUserSuccessPayload
  ): FetchPostUserSuccess => ({
    type: FETCH_POST_USER_SUCCESS,
    payload,
  });
  export const fetchUserFailure = (
    payload: FetchUserFailurePayload
  ): FetchUserFailure => ({
    type: FETCH_USER_FAILURE,
    payload,
  });