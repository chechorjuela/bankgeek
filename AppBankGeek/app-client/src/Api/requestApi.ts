
import axios from 'axios'
import {serviceUrl} from '../Helpers/serviceUrl';
const isLocal = process.env.REACT_APP_LOCAL === 'local'

const instance = axios.create({
  baseURL: serviceUrl.baseURL,
  timeout: 3000,
  params: {} // do not remove this, its added to add params later in the config
})

// Add a request interceptor
instance.interceptors.request.use(
  async config => {
    config.headers.common['Access-Control-Allow-Origin'] = '*'
    return config
  },
  error => {
    // Do something with request error
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  response => {
    return response
  },
  error => {
    return Promise.reject(error)
  }
)

export default {
  getData(action: string, data?: any) {
    let url = action
    data='';
    return instance.get(url, data)
  },
  postData(action: string, data?: any) {
    let url = action
    return instance.post(url, data)
  },
  putData(action: string, data?: any) {
    let url = `${
      isLocal
        ? process.env.REACT_APP_API_URL_DEV
        : process.env.REACT_APP_API_URL
    }`
    url += action
    return instance.put(url, data)
  },
  deleteData(action: string) {
    let url = `${
      isLocal
        ? process.env.REACT_APP_API_URL_DEV
        : process.env.REACT_APP_API_URL
    }`
    url += action
    return instance.delete(url)
  }
}
