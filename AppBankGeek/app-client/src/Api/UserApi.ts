import { logger } from '../Helpers/Utils/logger'
import requestApi from './requestApi'
import { serviceUrl } from '../Helpers/serviceUrl'
const getUsers = async () => {
  try {
    const action = `${serviceUrl.user}`;
    const result = await requestApi.getData(action)

    return result.data
  } catch (error) {
    logger('error.response', error)
    throw error
  }
}

const getProfileByUserId = async (userId: number) => {
  try {
    const action = `/profile/${userId}`
    const result = await requestApi.getData(action)

    return result.data
  } catch (error) {
    throw error
  }
}

const postUser = async (data: { createUserViewModel: any }) => {
  try {
    const action = `${serviceUrl.user}`;
    const result = await requestApi.postData(action, data.createUserViewModel)
    return result.data
  } catch (error) {
    logger('error post profile', error)
    throw error
  }
}

export default {
  getUsers,
  postUser
}
