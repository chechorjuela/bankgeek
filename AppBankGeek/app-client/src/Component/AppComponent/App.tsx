import React, { useState,useEffect } from "react";
import logo from "../../Source/logo.svg";
import { useForm, Controller } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { DataGrid } from '@material-ui/data-grid';
import "./App.css";
import { Button, Modal } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { fetchUserRequest,addUser } from "../../Store/UserRedux/actions";
import {
  getPendingSelector,
  getUsersSelector,
  getErrorSelector,
  getUserSelector,
} from "../../Store/UserRedux/selectors";
const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  
}));

const columns = [
  { field: 'id', headerName: 'ID', width: 90 },
  {
    field: 'userName',
    headerName: 'First name',
    width: 150,
    editable: false,
  },
  {
    field: 'firstNumber',
    headerName: 'First Number',
    width: 150,
    editable: true,
  },
  {
    field: 'secondNumber',
    headerName: 'Result',
    type: 'number',
    width: 110,
    editable: true,
  },
  {
    field: 'resultNumber',
    headerName: 'Result',
    type: 'number',
    width: 110,
    editable: true,
  },
  {
    field: 'booleanFibonacci',
    headerName: 'Fibonacci',
  

    width: 60
  },
];
function rand() {
  return Math.round(Math.random() * 20) - 10;
}
function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}
function App() {
  const dispatch = useDispatch();
  const [modalStyle] = React.useState(getModalStyle);
  const pending = useSelector(getPendingSelector);
  const users = useSelector(getUsersSelector);
  const error = useSelector(getErrorSelector);
  const { handleSubmit, control } = useForm();
  const dataSubmitted = useSelector(getUserSelector);
  const [loading,setLoading] = useState(false);
  const classes = useStyles();
  useEffect(() => {
    dispatch(fetchUserRequest());
  }, [])
  useEffect(() => {
    if(loading){
      if(dataSubmitted.booleanFibonacci){
        alert("The number is fibonacci")
      }else{
        alert("The number is not fibonacci")
      }
      setLoading(false);

    }
    
  }, [pending])
  const onSubmit = (data: any) => {
    dispatch(addUser(data));
    setLoading(true)
  };
  const handleClose = () => {
  };
  

  return (
    <div className="App-header">
      <form
        className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Controller
          name="userName"
          control={control}
          defaultValue=""
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TextField
              label="UserName"
              variant="filled"
              value={value}
              onChange={onChange}
              error={!!error}
              helperText={error ? error.message : null}
            />
          )}
          rules={{ required: "First Number required" }}
        />
        <Controller
          name="firstNumber"
          control={control}
          defaultValue=""
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TextField
              label="Second Number"
              variant="filled"
              value={value}
              onChange={onChange}
              error={!!error}
              helperText={error ? error.message : null}
            />
          )}
          rules={{ required: "Second Number required" }}
        />
        <Controller
          name="secondNumber"
          control={control}
          defaultValue=""
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TextField
              label="Second Number"
              variant="filled"
              value={value}
              onChange={onChange}
              error={!!error}
              helperText={error ? error.message : null}
            />
          )}
          rules={{ required: "First name required" }}
        />
        <Button type="submit" variant="contained" color="primary">
          Create
        </Button>
      </form>

      {pending ? (
        <div>Loading...</div>
      ) : error ? (
        <div>Error</div>
      ) : (
        <div style={{ height: 400, width: "80%" }}>
          <DataGrid
            rows={users}
            columns={columns}
            pageSize={5}
            checkboxSelection
            disableSelectionOnClick
          />
        </div>
      )}
  

    </div>
  );
}

export default App;
