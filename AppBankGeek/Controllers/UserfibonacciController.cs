﻿using BankGeek.Domain.Contract;
using BankGeek.Domain.Contract.Request;
using BankGeek.Domain.Contract.Response;
using BankGeek.Services.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBankGeek.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserfibonacciController : ControllerBase
    {
        protected IUserFibonacciService _userService;

        public UserfibonacciController(IUserFibonacciService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public ResponseBaseContract<List<ResponseFibonacciDto>> Index()
        {
            ResponseBaseContract<List<ResponseFibonacciDto>> response = new ResponseBaseContract<List<ResponseFibonacciDto>>();
            try
            {
                response.Data = _userService.GetAll();
            }
            catch (Exception ex) {
                response.Header.Message = ex.Message;
            }
            return response;
        }

        [HttpPost]
        public ResponseBaseContract<ResponseFibonacciDto> Create(RequestFibonacciDto requestDto) {
            ResponseBaseContract<ResponseFibonacciDto> response = new ResponseBaseContract<ResponseFibonacciDto>();
            try{
                response.Data = _userService.CreateFibonacci(requestDto);
            }
            catch(Exception ex) {
                response.Header.Message = ex.Message;
            }
            return response;
        }

        [HttpPut]
        public ResponseFibonacciDto Update(RequestFibonacciDto requestDto)
        {
            ResponseFibonacciDto responseFibonacciDto = new ResponseFibonacciDto();
            return responseFibonacciDto;
        }
        [HttpDelete]
        public ResponseFibonacciDto Delete(int id)
        {
            ResponseFibonacciDto responseFibonacciDto = new ResponseFibonacciDto();
            return responseFibonacciDto;
        }
    }
}
