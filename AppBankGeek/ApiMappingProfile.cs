﻿using AutoMapper;
using BankGeek.Domain.Contract.Request;
using BankGeek.Domain.Contract.Response;
using BankGeek.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBankGeek
{
    public class ApiMappingProfile: Profile
    {
        public ApiMappingProfile()
        {
            CreateMap<UserFibonacci, RequestFibonacciDto>().ReverseMap();
            CreateMap<UserFibonacci, ResponseFibonacciDto>().ReverseMap();
        }
    }
}
